# Standard Library imports
import os

# Package imports
import flask
import seeq

app = flask.Flask(__name__)

@app.route('/')
@app.route('/<user_id>')
def index(user_id=None):
    study_id = os.environ.get('SEEQ_STUDY_ID')
    try:
        study_id = int(study_id)
    except ValueError as e:
		  # In case study id cannot be converted to integer
        study_id = None
    except TypeError as e:
          # In case study id is non-convertible type
        study_id = None

    study_api_key = os.environ.get('SEEQ_STUDY_API_KEY')
     
    warnings = []

    if user_id is None:
        user_id = 'NO_USER_ID_PROVIDED'
        warnings.append('User id was not provided, using default.')

    if study_id is not None and study_api_key is not None and user_id is not None:
        # for demonstration purposes, if there's no user id provided, just set it to 0
        url = seeq.util.jwt_signed(study_id, user_id, study_api_key)
    else:
        warnings.append('Study id or study api key not defined (URL not generated).')
        url = '#'

    return flask.render_template('index.html', url=url, warnings=warnings)
