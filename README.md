# Example Seeq Study

This is an example of a simple website for directing users to your Seeq study.

It can be easily deployed to a free Heroku instance.

Visit [Seeq Research](http://research.seeq.io) for instructions on creating a study with Seeq.

## Step 1: Fork this repository

Go to [https://gitlab.com/seeqlabs/example-research](https://gitlab.com/seeqlabs/example-research) and click "Fork".

Clone your new repository to your computer:

```
git clone git@gitlab.com:<YOUR-USERNAME>/example-research.git
```

## Step 2: Make changes

Change templates/index.html and any other files to your liking.

Commit those changes:

```
git add -A
git commit -m "Updates for new study"
```

Pushing your changes to GitLab is useful, but optional:

```
git push 
```

## Step 3: Register for a *free* Heroku account

Register [here](https://signup.heroku.com)

## Step 4: Install Heroku CLI

You can find easy-to-follow instructions for MacOS, Linux, and Windows [here](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)

## Step 5: Set up Heroku

```
heroku login

heroku create <CHOOSE-AN-APP-NAME>

heroku config:set SEEQ_STUDY_ID=<INSERT-YOUR-STUDY-ID-HERE>
heroku config:set SEEQ_STUDY_API_KEY=<INSERT-YOUR-STUDY-API-KEY-HERE>
```

## Step 6: Deploy your study

Deploy

```
git push heroku master
```

Get app URL (under 'Web URL')

```
heroku apps:info
```

## Step 7: Send users to your Seeq study

Easily send users to Seeq by directing them to the app URL as follows:

```
<HEROKU-APP-URL>/<UNIQUE-USER-IDENTIFIER>
```

Feel free to try out this ^ URL in your browser.
